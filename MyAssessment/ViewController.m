//
//  ViewController.m
//  MyAssessment
//
//  Created by Aung Phyoe on 17/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import "ViewController.h"
#import "DateTableViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.backgroundColor) {
        self.view.backgroundColor=self.backgroundColor;
    }
    
    if (self.currentControllerType==ColorControllerType) {
        
        self.dataList=[[NSMutableArray alloc]initWithObjects:@"blue",@"red",@"green", nil];
        self.lblAction.hidden=NO;
        self.btnGoLast.hidden=YES;
        
        
        __block ViewController *this=self;
        self.didClickHandler=^(NSString* message){
            this.lblAction.text=message;
        
        };
        
        
    }else{
    
        self.lblAction.hidden=YES;
        self.btnGoLast.hidden=NO;

        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction:)];
       
    }
    
    if (self.dataList) {
        
        for (int i=0; i<[self.dataList count]; i++) {
            
            switch (i) {
                case 0:
                    self.lblOne.text=[self.dataList objectAtIndex:i];
                    break;
                case 1:
                    self.lblTwo.text=[self.dataList objectAtIndex:i];
                    break;

                case 2:
                    self.lblThree.text=[self.dataList objectAtIndex:i];
                    break;

                    
                default:
                    break;
            }
            
        }
        
    }
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction:(UIButton *)sender {
    

        
        self.didClickHandler(@"None");
        [self.navigationController popViewControllerAnimated:YES];
        
    
    
    
}

- (IBAction)firstButtonAction:(UIButton *)sender {
    
    if (self.currentControllerType==ColorControllerType) {
        [self pushSecondViewWithColor:[UIColor blueColor]];
    }else{
    
        self.didClickHandler(@"ONE");
        [self.navigationController popViewControllerAnimated:YES];
    
    }
    
    
}

- (IBAction)secondButtonAction:(UIButton *)sender {
    
    if (self.currentControllerType==ColorControllerType) {
        [self pushSecondViewWithColor:[UIColor redColor]];
    }else{
        
        self.didClickHandler(@"TWO");
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

- (IBAction)thirdButtonAction:(UIButton *)sender {
  
    if (self.currentControllerType==ColorControllerType) {
        [self pushSecondViewWithColor:[UIColor greenColor]];
    }else{
        
        self.didClickHandler(@"THREE");
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

- (IBAction)golastAction:(UIButton *)sender {
    
    DateTableViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"DateTableViewControllerID"];
     [self.navigationController pushViewController:controller animated:YES];
}

-(void)pushSecondViewWithColor:(UIColor*)color{

    ViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"ViewControllerID"];
    controller.currentControllerType=NumberControllerType;
    controller.backgroundColor=color;
    controller.didClickHandler=self.didClickHandler;
    controller.dataList=[[NSMutableArray alloc]initWithObjects:@"ONE",@"TWO",@"THREE", nil];
    [self.navigationController pushViewController:controller animated:YES];

}
@end
