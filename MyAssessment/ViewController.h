//
//  ViewController.h
//  MyAssessment
//
//  Created by Aung Phyoe on 17/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    ColorControllerType ,
    NumberControllerType
};
typedef NSUInteger ControllerType;


@interface ViewController : UIViewController{


    

}
@property (retain, nonatomic) NSMutableArray *dataList;
@property (retain, nonatomic) UIColor *backgroundColor;
@property (assign) ControllerType currentControllerType;
@property (nonatomic, copy)void (^didClickHandler)(NSString *message);

@property (weak, nonatomic) IBOutlet UILabel *lblOne;
@property (weak, nonatomic) IBOutlet UILabel *lblTwo;
@property (weak, nonatomic) IBOutlet UILabel *lblThree;
@property (weak, nonatomic) IBOutlet UILabel *lblAction;
@property (weak, nonatomic) IBOutlet UIButton *btnGoLast;

- (IBAction)firstButtonAction:(UIButton *)sender;
- (IBAction)secondButtonAction:(UIButton *)sender;
- (IBAction)thirdButtonAction:(UIButton *)sender;
- (IBAction)golastAction:(UIButton *)sender;

@end

