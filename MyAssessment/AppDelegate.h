//
//  AppDelegate.h
//  MyAssessment
//
//  Created by Aung Phyoe on 17/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

